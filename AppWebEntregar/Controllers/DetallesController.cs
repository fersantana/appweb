﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppWebEntregar.Models;

namespace AppWebEntregar.Controllers
{
    public class DetallesController : Controller
    {
        private Datos1Entities db = new Datos1Entities();

        // GET: Detalles
        public ActionResult Index()
        {
            var detalles = db.Detalles.Include(d => d.Vehiculo);
            return View(detalles.ToList());
        }

        // GET: Detalles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detalles detalles = db.Detalles.Find(id);
            if (detalles == null)
            {
                return HttpNotFound();
            }
            return View(detalles);
        }

        // GET: Detalles/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.Vehiculo, "id", "Marca");
            return View();
        }

        // POST: Detalles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Marca,T_trans,Kilometraje,Combustible,Color,Modelo")] Detalles detalles)
        {
            if (ModelState.IsValid)
            {
                db.Detalles.Add(detalles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id = new SelectList(db.Vehiculo, "id", "Marca", detalles.id);
            return View(detalles);
        }

        // GET: Detalles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detalles detalles = db.Detalles.Find(id);
            if (detalles == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.Vehiculo, "id", "Marca", detalles.id);
            return View(detalles);
        }

        // POST: Detalles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Marca,T_trans,Kilometraje,Combustible,Color,Modelo")] Detalles detalles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detalles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id = new SelectList(db.Vehiculo, "id", "Marca", detalles.id);
            return View(detalles);
        }

        // GET: Detalles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detalles detalles = db.Detalles.Find(id);
            if (detalles == null)
            {
                return HttpNotFound();
            }
            return View(detalles);
        }

        // POST: Detalles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Detalles detalles = db.Detalles.Find(id);
            db.Detalles.Remove(detalles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
