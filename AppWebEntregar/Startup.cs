﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppWebEntregar.Startup))]
namespace AppWebEntregar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
